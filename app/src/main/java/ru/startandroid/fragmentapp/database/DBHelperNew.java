package ru.startandroid.fragmentapp.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DBHelperNew extends SQLiteOpenHelper {

    public DBHelperNew(Context context) {
        // конструктор суперкласса
        super(context, "Redmine", null, 1);


    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        Log.d("MyLogs", "--- onCreate database ---");
        // создаем таблицу с полями

        db.execSQL("create table project ("
                + "id integer primary key autoincrement,"
                + "proj_name text" + ");");

        db.execSQL("create table issues ("
                + "id_issue integer primary key autoincrement,"
                + "subject text,"
                + "id_parent_project" + ");");

        db.execSQL("create table issues_detail ("
                + "id_issue integer primary key autoincrement,"
                + "priority text," + "author text," + "description text,"
                + "status" + ");");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}

