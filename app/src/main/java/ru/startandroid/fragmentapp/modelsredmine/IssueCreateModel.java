package ru.startandroid.fragmentapp.modelsredmine;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class IssueCreateModel {
    @SerializedName("project_id")
    @Expose
    private Integer projectid;
    @SerializedName("subject")
    @Expose
    private String subject;
    @SerializedName("priority_id")
    @Expose
    private Integer priorityid;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("status")
    @Expose
    private Integer status;

    public IssueCreateModel(Integer projectid, String subject, Integer priorityid, String description,Integer status) {
        this.projectid = projectid;
        this.subject = subject;
        this.priorityid = priorityid;
        this.description = description;
        this.status = status;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getProjectid() {
        return projectid;
    }

    public void setProjectid(Integer projectid) {
        this.projectid = projectid;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public Integer getPriorityid() {
        return priorityid;
    }

    public void setPriorityid(Integer priorityid) {
        this.priorityid = priorityid;
    }

    @Override
    public String toString() {
        return "IssueModelCreate{" +
                "projectid=" + projectid +
                "description =" + description +
                ", subject='" + subject + '\'' +
                ", priorityid=" + priorityid +
                '}';
    }
}
