package ru.startandroid.fragmentapp.modelsredmine;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class IssuePostModel {
    @SerializedName("issue")
    @Expose
    private IssueCreateModel issue = null;

    public IssuePostModel(IssueCreateModel issue) {
        this.issue = issue;
    }

    public IssueCreateModel getIssue() {
        return issue;
    }

    public void setIssue(IssueCreateModel issue) {
        this.issue = issue;
    }

    @Override
    public String toString() {
        return "IssueModelCreate{" +
                "projectid=" + issue.getProjectid() + " " +
                ", subject='" + issue.getSubject() + " " + '\'' +
                ", priorityid=" + issue.getPriorityid() +
                '}';
    }
}
