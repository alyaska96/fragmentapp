package ru.startandroid.fragmentapp.ui;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import ru.startandroid.fragmentapp.R;
import ru.startandroid.fragmentapp.models.ProjectModel;

import java.util.List;

public class ProjectGridAdapter extends BaseAdapter {
    Context ctx;
    private List<ProjectModel> mData;
    private LayoutInflater mInflater;


    public ProjectGridAdapter(Context context, List<ProjectModel> mData) {
        ctx = context;
        this.mData = mData;
        mInflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return mData.size();
    }

    @Override
    public Object getItem(int position) {
        return mData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            view = mInflater.inflate(R.layout.grid_item, parent, false);
        }

        ProjectModel p = getProj(position);
        //fill View
        ((TextView) view.findViewById(R.id.tvGridId)).setText(String.valueOf(p.getId()));
        ((TextView) view.findViewById(R.id.tvGridName)).setText(p.getName());
        Log.d("MyLogs", "grid adapter" + p.getName() + "  " + p.getId());

        return view;
    }

    // project by position
    public ProjectModel getProj(int position) {
        return ((ProjectModel) getItem(position));
    }

    public void filterList(List<ProjectModel> foundProjectListForRV) {
        this.mData = foundProjectListForRV;
        notifyDataSetChanged();
    }

}