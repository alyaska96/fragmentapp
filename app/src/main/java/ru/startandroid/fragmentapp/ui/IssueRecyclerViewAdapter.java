package ru.startandroid.fragmentapp.ui;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import ru.startandroid.fragmentapp.R;
import ru.startandroid.fragmentapp.models.IssueModel;

public class IssueRecyclerViewAdapter extends RecyclerView.Adapter<IssueRecyclerViewAdapter.ViewHolder> {

    private List<IssueModel> mData;
    private LayoutInflater mInflater;
    private ItemClickListener mClickListener;

    // data is passed into the constructor

    public IssueRecyclerViewAdapter(Context context, List<IssueModel> data) {
        this.mInflater = LayoutInflater.from(context);
        this.mData = new ArrayList<>(data);
    }

    // inflates the row layout from xml when needed

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.issue_item, parent, false);//???
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int positionI) {
        IssueModel issue = mData.get(positionI);
        viewHolder.myIssueTracker.setText(issue.getSubject());
        viewHolder.myIDIssue.setText(String.valueOf(issue.getId()));

    }


    // total number of rows
    @Override
    public int getItemCount() {
        return mData.size();
    }

    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView myIssueTracker;
        TextView myIDIssue;

        ViewHolder(View itemView) {
            super(itemView);
            myIssueTracker = itemView.findViewById(R.id.tvTracker);
            myIDIssue = itemView.findViewById(R.id.tvIdIssues);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());
        }
    }

    // convenience method for getting data at click position
   public IssueModel getItem(int id) {
        return mData.get(id);
    }

    // allows clicks events to be caught
    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }

    public void filterList(List<IssueModel> foundProjectListForRV) {
        this.mData = foundProjectListForRV;
        notifyDataSetChanged();
    }
}