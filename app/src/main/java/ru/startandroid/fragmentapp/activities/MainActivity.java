package ru.startandroid.fragmentapp.activities;

import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnPageChange;
import ru.startandroid.fragmentapp.R;
import ru.startandroid.fragmentapp.fragments.CreateNewIssueFragment;
import ru.startandroid.fragmentapp.fragments.DetailIssueFragment;
import ru.startandroid.fragmentapp.fragments.GridProjectFragment;
import ru.startandroid.fragmentapp.fragments.IssuesFragment;
import ru.startandroid.fragmentapp.fragments.ProjectFragment;
import ru.startandroid.fragmentapp.retrofit.TheApplication;
import ru.startandroid.fragmentapp.ui.MyFragmentPagerAdapter;
import ru.terrakok.cicerone.Navigator;
import ru.terrakok.cicerone.Router;
import ru.terrakok.cicerone.android.SupportFragmentNavigator;

public class MainActivity extends FragmentActivity implements ProjectFragment.onClickProjectListener, IssuesFragment.onClickIssueListener, GridProjectFragment.onClickProjectListener {

    final String LOG_TAG = "MyLogs";
    final String ISSUES_SCREEN = "ISSUE_SCREEN";
    final String GRID_SCREEN = "GRID_SCREEN";
    final String PROJECT_SCREEN = "PROJECT_SCREEN";
    static final String TAG = "myLogs";
    public static final String KEY = "87fe5be4b9f562309e5bc8ab7eb8847f64499ca1";

    IssuesFragment issuesFragment;
    DetailIssueFragment detailIssueFragment;
    String idProject;
    Router router;

    @BindView(R.id.pager)
    ViewPager pager;
    PagerAdapter pagerAdapter;


    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        pagerAdapter = new MyFragmentPagerAdapter(getSupportFragmentManager());
        pager.setAdapter(pagerAdapter);

        router = TheApplication.INSTANCE.getRouter();
        router.navigateTo(PROJECT_SCREEN);
        issuesFragment = new IssuesFragment();
        detailIssueFragment = new DetailIssueFragment();

    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(TAG, "MainActivity on Start");
    }

    protected void onResume() {
        super.onResume();
        TheApplication.INSTANCE.getNavigatorHolder().setNavigator(navigator);
        NetworkStateReceiver networkStateReceiver = new NetworkStateReceiver();
        registerReceiver(networkStateReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
    }


    @OnPageChange(R.id.pager)
    public void onPageSelected(int i) {
        Log.d("MyLogs", "on Page selected ");
        switch (i) {
            case 0: {
                router.navigateTo(PROJECT_SCREEN);
                break;
            }
            case 1:
                router.navigateTo(GRID_SCREEN);
                break;
        }
    }

    @Override
    public void chooseProject(String s) {
        Log.d(LOG_TAG, "in MainActivity id of project " + s);
        idProject = s;
        pager.setVisibility(View.INVISIBLE);
        Bundle bundle = new Bundle();
        bundle.putString("message", idProject);
        issuesFragment.setArguments(bundle);
        router.navigateTo(ISSUES_SCREEN);

    }

    @Override
    public void chooseIssue(String id, String author, String description, String subject, String status, String priority) {
        Bundle bID = new Bundle();
        bID.putString("idIssue", id);
        bID.putString("Author", author);
        bID.putString("Description", description);
        bID.putString("Subject", subject);
        bID.putString("Status", status);
        bID.putString("Priority", priority);
        detailIssueFragment.setArguments(bID);
        detailIssueFragment.show(getSupportFragmentManager(), "detail");
    }

    @Override
    public void createNewIssue() {
        CreateNewIssueFragment createNewIssueFragment = new CreateNewIssueFragment();
        createNewIssueFragment.show(getSupportFragmentManager(), "create issue");
    }

    @Override
    public void chooseGridProject(String s) {
        Log.d(LOG_TAG, "in MainActivity id of project GridFragment" + s);
        pager.setVisibility(View.INVISIBLE);
        idProject = s;
        Bundle bundle = new Bundle();
        bundle.putString("message", idProject);
        issuesFragment.setArguments(bundle);
        router.navigateTo(ISSUES_SCREEN);

    }

    private Navigator navigator = new SupportFragmentNavigator(getSupportFragmentManager(),
            R.id.frgmCont) {
        @Override
        protected Fragment createFragment(String screenKey, Object data) {
            switch (screenKey) {
                case PROJECT_SCREEN:
                    return new ProjectFragment();
                case GRID_SCREEN:
                    return new GridProjectFragment();
                case ISSUES_SCREEN:
                    return issuesFragment;
                default:
                    throw new RuntimeException("Unknown screen key!");
            }
        }

        @Override
        protected void showSystemMessage(String message) {
            Toast.makeText(MainActivity.this, message, Toast.LENGTH_SHORT).show();
        }

        @Override
        protected void exit() {
            finish();
        }

    };


    @Override
    protected void onPause() {
        super.onPause();
        TheApplication.INSTANCE.getNavigatorHolder().removeNavigator();
    }
}

