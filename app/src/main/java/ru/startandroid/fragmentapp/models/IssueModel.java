package ru.startandroid.fragmentapp.models;
    public class IssueModel {
        private int id;
        private PriorityModel priority;
        private StatusModel status;
        private AuthorModel author;
        private String description;
        private String subject;
        private TrackerModel tracker;
        private Integer projectId;
        private ProjectIForIssueModel project;

        public IssueModel(PriorityModel priority, StatusModel status, String description, String subject, TrackerModel tracker, Integer projectId, ProjectIForIssueModel project) {
            this.priority = priority;
            this.status = status;
            // this.author = author;
            this.tracker = tracker;
            this.description = description;
            this.subject = subject;
            this.projectId = projectId;
            this.project = project;
        }

        public IssueModel() {
        }

        @Override
        public String toString() {
            return "IssueModel{" +
                    ", project id=" + projectId + " " +
                    ", priority=" + priority.getName() +" "+ priority.getId() +
                    ", status=" + status.getName() + " "+ status.getId() +
                    ", tracker= " + tracker.getName() + " " + tracker.getId() +
                    ", description='" + description + '\'' +
                    ", subject='" + subject + '\'' +
                    '}';
        }
        public TrackerModel getTracker() {
            return tracker;
        }

        public void setTracker(TrackerModel tracker) {
            this.tracker = tracker;
        }

        public ProjectIForIssueModel getProject() {
            return project;
        }

        public void setProject(ProjectIForIssueModel project) {
            this.project = project;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public PriorityModel getPriority() {
            return priority;
        }

        public void setPriority(PriorityModel priority) {
            this.priority = priority;
        }

        public StatusModel getStatus() {
            return status;
        }

        public void setStatus(StatusModel status) {
            this.status = status;
        }

        public AuthorModel getAuthor() {
            return author;
        }

        public void setAuthor(AuthorModel author) {
            this.author = author;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getSubject() {
            return subject;
        }

        public void setSubject(String subject) {
            this.subject = subject;
        }

        public Integer getProjectId() {
            return projectId;
        }

        public void setProjectId(Integer projectId) {
            this.projectId = projectId;
        }

    }
