package ru.startandroid.fragmentapp.retrofit;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;
import ru.startandroid.fragmentapp.modelsredmine.ProjectModelRequest;

public interface ProjectRedmineApi {
    @GET("projects.json")
    Call<ProjectModelRequest> getData(@Query("key") String resourceKey);
}


