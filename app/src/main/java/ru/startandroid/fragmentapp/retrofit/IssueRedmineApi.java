package ru.startandroid.fragmentapp.retrofit;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;
import ru.startandroid.fragmentapp.modelsredmine.IssueModelRequest;
import ru.startandroid.fragmentapp.modelsredmine.IssuePostModel;

public interface IssueRedmineApi {
    //https://rm.stagingmonster.com/projects/677/issues.json?offset=0&limit=15&key=87fe5be4b9f562309e5bc8ab7eb8847f64499ca1
    @GET("projects/{project}/issues.json")
    Call<IssueModelRequest> getData(@Path("project") String project, @Query("offset") int offset, @Query("limit") int limit, @Query("key") String resourceKey);
    @POST("projects/847/issues.json")
    Call<IssuePostModel> createIssue (@Body IssuePostModel issue, @Query("key") String resourceKey);
}
