package ru.startandroid.fragmentapp.retrofit;

import android.app.Application;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import ru.terrakok.cicerone.Cicerone;
import ru.terrakok.cicerone.NavigatorHolder;
import ru.terrakok.cicerone.Router;

public class TheApplication extends Application {
    private static ProjectRedmineApi projectRedmineApi;
    private static IssueRedmineApi issueRedmineApi;
    private Retrofit retrofit;

    public static TheApplication INSTANCE;
    private Cicerone<Router> cicerone;


    @Override
    public void onCreate() {
        super.onCreate();
        INSTANCE = this;
        initCicerone();
        initRetrofit();
        projectRedmineApi = retrofit.create(ProjectRedmineApi.class);
        issueRedmineApi = retrofit.create(IssueRedmineApi.class);
    }

    private void initCicerone() {
        cicerone = Cicerone.create();
    }

    public NavigatorHolder getNavigatorHolder() {
        return cicerone.getNavigatorHolder();
    }

    public Router getRouter() {
        return cicerone.getRouter();
    }

    private void initRetrofit() {
        retrofit = new Retrofit.Builder()
                .baseUrl("https://rm.stagingmonster.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    public static ProjectRedmineApi getApi() {

        return projectRedmineApi;
    }

    public static IssueRedmineApi getIssueApi() {

        return issueRedmineApi;
    }
}


