package ru.startandroid.fragmentapp.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnEditorAction;
import butterknife.OnTextChanged;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ru.startandroid.fragmentapp.R;
import ru.startandroid.fragmentapp.activities.MainActivity;
import ru.startandroid.fragmentapp.models.PriorityModel;
import ru.startandroid.fragmentapp.models.StatusModel;
import ru.startandroid.fragmentapp.models.TrackerModel;
import ru.startandroid.fragmentapp.modelsredmine.IssueCreateModel;
import ru.startandroid.fragmentapp.modelsredmine.IssuePostModel;
import ru.startandroid.fragmentapp.retrofit.TheApplication;

public class CreateNewIssueFragment extends DialogFragment {
    View view;
    Context context;
    PriorityModel priorityModel;
    StatusModel statusModel;
    TrackerModel trackerModel;
    private final Integer projectId = 847;
    String new_name;
    @BindView(R.id.tvCreateName)
    TextView tvCreateName;
    @BindView(R.id.etName)
    EditText etName;
    @BindView(R.id.tvCreateTracker)
    TextView tvCreateTracker;
    @BindView(R.id.spinnerTracker)
    Spinner spinnerTracker;
    @BindView(R.id.tvCreateDescription)
    TextView tvCreateDescription;
    @BindView(R.id.etDescription)
    EditText etDescription;
    @BindView(R.id.tvCreatePriority)
    TextView tvCreatePriority;
    @BindView(R.id.spinnerPriority)
    Spinner spinnerPriority;
    @BindView(R.id.tvCreateStatus)
    TextView tvCreateStatus;
    @BindView(R.id.spinnerStatus)
    Spinner spinnerStatus;
    @BindView(R.id.addButton)
    Button addButton;
    Unbinder unbinder;

    public CreateNewIssueFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_create_new_issue, null);
        getDialog().setTitle("New Issue Create: ");
        context = view.getContext();
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        getDialog().getWindow().setLayout(800, 950);

        addButton.setEnabled(false);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(context,
                R.array.priority_array, android.R.layout.simple_spinner_item);
        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        spinnerPriority.setAdapter(adapter);
        ArrayAdapter<CharSequence> stAdapter = ArrayAdapter.createFromResource(context,
                R.array.status_array, android.R.layout.simple_spinner_item);
        // Specify the layout to use when the list of choices appears
        stAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        spinnerStatus.setAdapter(stAdapter);
        ArrayAdapter<CharSequence> trackerAdapter = ArrayAdapter.createFromResource(context,
                R.array.tracker_array, android.R.layout.simple_spinner_item);
        // Specify the layout to use when the list of choices appears
        trackerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        spinnerTracker.setAdapter(trackerAdapter);

    }

    @OnTextChanged(R.id.etName)
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        new_name = s.toString();
        if (new_name.contains("   ") || new_name.startsWith(" ")) {
            new_name = "";
        }
        Log.d("MyLogs", "string " + new_name);
        if (new_name.length() == 0) {

            addButton.setEnabled(false);
        } else {

            addButton.setEnabled(true);
        }
    }
    @OnEditorAction(R.id.etDescription)
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        if (event != null && event.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
            return true;
        }
        return false;
    }


    public void convertPriorityId() {

        switch (String.valueOf(spinnerPriority.getSelectedItem())) {
            case "Low":
                priorityModel.setId(3);
                break;
            case "Normal":
                priorityModel.setId(4);
                break;
            case "Hight":
                priorityModel.setId(5);
                break;
            case "Urgent":
                priorityModel.setId(6);
                break;
            case "Immediate":
                priorityModel.setId(7);
                break;
        }
    }

    public void convertStatusId() {

        switch (String.valueOf(spinnerStatus.getSelectedItem())) {
            case "New":
                statusModel.setId(1);
                break;
            case "Assigned":
                statusModel.setId(2);
                break;
            case "Resolved":
                statusModel.setId(3);
                break;
            case "Feedback":
                statusModel.setId(4);
                break;
            case "In progress":
                statusModel.setId(8);
                break;
            case "Ready for review":
                statusModel.setId(9);
                break;
            case "Awaiting client feedback":
                statusModel.setId(10);
                break;
            case "In queue":
                statusModel.setId(11);
                break;
            case "in Testing":
                statusModel.setId(12);
                break;
            case "Completed":
                statusModel.setId(13);
                break;
        }
    }

    public void convertTrackerId() {

        switch (String.valueOf(spinnerTracker.getSelectedItem())) {
            case "Bug":
                trackerModel.setId(1);
                break;
            case "Task":
                trackerModel.setId(4);
                break;
        }
    }

    public void fillingData() {
        priorityModel = new PriorityModel((String) spinnerPriority.getSelectedItem());
        statusModel = new StatusModel(String.valueOf(spinnerStatus.getSelectedItem()));
        trackerModel = new TrackerModel((String) spinnerTracker.getSelectedItem());
        convertTrackerId();
        convertStatusId();
        convertPriorityId();
        String subject = (String.valueOf(etName.getText()));
        String description = (String.valueOf(etDescription.getText()));
        IssueCreateModel issueModelCreate = new IssueCreateModel(projectId, subject, priorityModel.getId(), description, statusModel.getId());
        Log.d("MyLogs", "issue model for redmine " + issueModelCreate.toString());
        IssuePostModel issueModelPost = new IssuePostModel(issueModelCreate);
        Log.d("MyLogs", "issue model for Post" + issueModelPost.toString());
        TheApplication.getIssueApi().createIssue(issueModelPost, MainActivity.KEY).enqueue(new Callback<IssuePostModel>() {
            @Override
            public void onResponse(Call<IssuePostModel> call, Response<IssuePostModel> response) {

            }

            @Override
            public void onFailure(Call<IssuePostModel> call, Throwable t) {

            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.addButton)
    public void onViewClicked() {
        fillingData();
        dismiss();
    }
}
