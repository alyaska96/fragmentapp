package ru.startandroid.fragmentapp.fragments;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v4.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ru.startandroid.fragmentapp.activities.MainActivity;
import ru.startandroid.fragmentapp.database.DBHelperNew;
import ru.startandroid.fragmentapp.R;
import ru.startandroid.fragmentapp.models.IssueModel;
import ru.startandroid.fragmentapp.modelsredmine.IssueModelRequest;
import ru.startandroid.fragmentapp.retrofit.TheApplication;
import ru.startandroid.fragmentapp.ui.IssueRecyclerViewAdapter;

public class IssuesFragment extends Fragment implements IssueRecyclerViewAdapter.ItemClickListener {

    private List<IssueModel> issueALL = new ArrayList<>();
    String idIssue, priority, status, subject, description, author, idS;
    final static String LOG_TAG = "MyLogsDBIssue";

    int totalCount;

    Unbinder unbinder;
    IssueRecyclerViewAdapter issueAdapter;
    View view;
    Context context;

    @BindView(R.id.rvIssues)
    RecyclerView rvIssues;

    @BindView(R.id.imageButton2)
    ImageButton addBtn;

    @BindView(R.id.nextBtn)
    Button btnNext;

    onClickIssueListener onClickIssueListener;
    DBHelperNew dbHelper;
    SQLiteDatabase db;

    List<Integer> idParent;
    List<Integer> idIssueParent;
    boolean flag = false;


    public IssuesFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        issueALL.clear();
        String idS = this.getArguments().getString("message");
        if (idS.contains("847")) {
            addBtn.setVisibility(View.VISIBLE);
        }
        Log.d("MyLogs", "id of project in IssueFragment" + idS);
        view = inflater.inflate(R.layout.fragment_issues, null);
        context = view.getContext();
        unbinder = ButterKnife.bind(this, view);
        dbHelper = new DBHelperNew(context);
        db = dbHelper.getWritableDatabase();
        Cursor c = db.query("issues", null, null, null, null, null, null);
        logCursor(c);
        return view;
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        newHttpRequest();
    }

    @Override
    public void onResume() {
        super.onResume();

    }


    @Override
    public void onAttach(Context activity) {
        super.onAttach(activity);
        try {
            onClickIssueListener = (onClickIssueListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement onClickIssueListener");
        }
    }


    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onItemClick(View view, int position) {
        if (ProjectFragment.isOnline(context)) {
            idIssue = String.valueOf(issueAdapter.getItem(position).getId());
            priority = String.valueOf(issueAdapter.getItem(position).getPriority().getName());
            description = String.valueOf(issueAdapter.getItem(position).getDescription());
            status = String.valueOf(issueAdapter.getItem(position).getStatus().getName());
            author = String.valueOf(issueAdapter.getItem(position).getAuthor().getName());
            subject = String.valueOf(issueAdapter.getItem(position).getSubject());
            issueDetailDB();
            onClickIssueListener.chooseIssue(idIssue, author, description, subject, status, priority);
        } else {
            idIssue = String.valueOf(issueAdapter.getItem(position).getId());
            innerJoinTable();
            onClickIssueListener.chooseIssue(idIssue, author, description, subject, status, priority);
        }
    }

    public void newHttpRequest() {
        TheApplication.getIssueApi().getData(idS, issueALL.size(), 15, MainActivity.KEY).enqueue(new Callback<IssueModelRequest>() {
            @Override
            public void onResponse(Call<IssueModelRequest> call, Response<IssueModelRequest> response) {
                issueALL.addAll(response.body().getIssues());
                Log.d("MyLogs", "issueALL   " + issueALL.size());
                setIssueAdapter(issueALL);
                issueAdapter.notifyDataSetChanged();
                totalCount = response.body().getTotalCount();
                Log.d("MyLogs", "issueALL response  " + response.body().getTotalCount());
                Log.d("MyLogs", "RecyclerViewIssues  " + issueAdapter.getItemCount());
                issueDB();

            }

            @Override
            public void onFailure(Call<IssueModelRequest> call, Throwable t) {
                //Toast.makeText(context, "An error occurred during networking", Toast.LENGTH_SHORT).show();
                fillingIssuesfromDB();
                setIssueAdapter(issueALL);
                Log.d("MyLogs", "Issues unsuccuses  " + call.request().url());
            }
        });

    }

    public void setIssueAdapter(List<IssueModel> mData) {

        issueAdapter = new IssueRecyclerViewAdapter(context, mData);
        LinearLayoutManager llm = new LinearLayoutManager(context);//
        rvIssues.setLayoutManager(llm);
        issueAdapter.setClickListener(this);
        rvIssues.setAdapter(issueAdapter);

    }

    @Override
    public void onPause() {
        super.onPause();
        issueALL.clear();
    }


    void logCursor(Cursor c) {
        if (c != null) {
            if (c.moveToFirst()) {
                String str;
                do {
                    str = "";
                    for (String cn : c.getColumnNames()) {
                        str = str.concat(cn + " = " + c.getString(c.getColumnIndex(cn)) + "; ");
                    }
                    Log.d(LOG_TAG, str);
                } while (c.moveToNext());
            }
        } else
            Log.d(LOG_TAG, "Cursor is null");
    }

    public void issueDetailDB() {
        //    ifParentIssue();
        db = dbHelper.getWritableDatabase();
        Log.d(LOG_TAG, "id parent issue " + idIssue);
        Log.d(LOG_TAG, "flag in Details " + flag);
//        if (ProjectFragment.isOnline(context) && flag == true) {
//            Log.d(LOG_TAG, "--- Clear table issue : ---");
//            int clearCount = db.delete("issues", null, null);
//            Log.d(LOG_TAG, "deleted rows count = " + clearCount);
//        }
        // checking if there are some content in db
        Cursor c = db.query("issues_detail", null, null, null, null, null, null);
        //    if (c.getCount() == 0) {
        ContentValues cv = new ContentValues();
        // filling our table with data
        cv.put("id_issue", idIssue);
        cv.put("author", author);
        cv.put("description", description);
        cv.put("status", status);
        cv.put("priority", priority);
        db.insert("issues_detail", null, cv);
        //      }
        Log.d(LOG_TAG, "--- Все записи Issue Details---");
        c = db.query("issues_detail", null, null, null, null, null, null);
        logCursor(c);
        c.close();
        dbHelper.close();
        //      innerJoinTable();
    }

    public void innerJoinTable() {
        db = dbHelper.getWritableDatabase();
        List<String> statusF = new ArrayList();
        List<String> authorF = new ArrayList();
        List<String> subjectF = new ArrayList();
        List<String> descriptionF = new ArrayList();
        List<String> priorityF = new ArrayList();
        List<Integer> idIssueF = new ArrayList();
        List<Integer> idParentProjectF = new ArrayList();
        // выводим в лог данные по должностям
        Log.d(LOG_TAG, "--- Table issues ---");
        Cursor c = db.query("issues", null, null, null, null, null, null);
        logCursor(c);
        c.close();
        Log.d(LOG_TAG, "--- ---");
        // выводим в лог данные по людям
        Log.d(LOG_TAG, "--- Table issues_detail ---");
        c = db.query("issues_detail", null, null, null, null, null, null);
        logCursor(c);
        c.close();
        Log.d(LOG_TAG, "----------- ---------");

        // выводим результат объединения
        // используем rawQuery
        Log.d(LOG_TAG, "--- INNER JOIN with rawQuery---");
        String sqlQuery = "select ISD.author as Author, ISD.description as Description, ISD.priority as Priority, ISD.status as Status, ISU.subject as Subject, ISU.id_parent_project as ParentProject, ISU.id_issue as IdIssue "
                + "from issues as ISU "
                + "inner join issues_detail as ISD "
                + "on ISU.id_issue = ISD.id_issue "
                + "where subject != ?";
        c = db.rawQuery(sqlQuery, new String[]{""});
        while (c.moveToNext()) {
            String uname = c.getString(c.getColumnIndex("Subject"));
            subjectF.add(uname);
            String uId = c.getString(c.getColumnIndex("IdIssue"));
            idIssueF.add(Integer.valueOf(uId));
            String uParentProject = c.getString(c.getColumnIndex("ParentProject"));
            idParentProjectF.add(Integer.valueOf(uParentProject));
            String uStatus = c.getString(c.getColumnIndex("Status"));
            statusF.add(uStatus);
            String uAuthor = c.getString(c.getColumnIndex("Author"));
            authorF.add(uAuthor);
            String uPriority = c.getString(c.getColumnIndex("Priority"));
            priorityF.add(uPriority);
            String uDescription = c.getString(c.getColumnIndex("Description"));
            descriptionF.add(uDescription);
        }
        // CHECK THIS
        for (int m = 0; m < idIssueF.size(); m++) {   //?????? check this function
            Log.d("MyLogsP", "--- ID Parent Issue + idIssue---" + Integer.valueOf(idIssue) + " " + idIssueF.get(m) + " equals or not?  " + ((idIssueF.get(m)).equals(Integer.valueOf(idIssue))));
            if ((idIssueF.get(m)).equals(Integer.valueOf(idIssue))) {
                Log.d("MyLogsP", "--- in cycle for if idParent project equals idS ---");
                Log.d("MyLogsP", "--- IDS + idIssueParent---" + idIssue + "  " + (String.valueOf(idIssueF.get(m))));
                status = statusF.get(m);
                priority = priorityF.get(m);
                description = descriptionF.get(m);
                author = authorF.get(m);
                subject = subjectF.get(m);
//                IssueModel mv = new IssueModel();
//                mv.setSubject(subjectF.get(m));
//                mv.setId(idIssueF.get(m));
//                issueALL.add(mv);
                Log.d("MyLogsP", "--- detail information from DB ---" + status + "  " + priority + "  " + subject + "  " + author);
            } else {
                Toast.makeText(context, "No loading information", Toast.LENGTH_SHORT).show();
            }
        }
        logCursor(c);
        c.close();
        Log.d(LOG_TAG, "--- ---");
        dbHelper.close();

    }

    public void issueDB() {
        ifParentId();
        db = dbHelper.getWritableDatabase();
        Log.d(LOG_TAG, "flag " + flag);
        Log.d(LOG_TAG, "ids " + idS);
        if (ProjectFragment.isOnline(context) && flag == true) {
            Log.d(LOG_TAG, "--- Clear table issue : ---");
            int clearCount = db.delete("issues", null, null);
            Log.d(LOG_TAG, "deleted rows count = " + clearCount);
        }
        // checking if there are some content in db
//        Cursor c = db.query("issues", null, null, null, null, null, null);
//        if (c.getCount() == 0 && flag==true) {
        ContentValues cv = new ContentValues();
        for (int i = 0; i < issueALL.size(); i++) {
            cv.put("id_issue", issueALL.get(i).getId());
            cv.put("subject", issueALL.get(i).getSubject());
            cv.put("id_parent_project", idS);
            db.insert("issues", null, cv);
        }
        Log.d(LOG_TAG, "--- Все записи Issue Table---");
        Cursor c = db.query("issues", null, null, null, null, null, null);
        logCursor(c);
        c.close();
        dbHelper.close();
    }

//    public void ifParentIssue() {
//        db = dbHelper.getWritableDatabase();
//        idIssueParent = new ArrayList();
//        flag = false;
//        Cursor cr;
//        cr = db.query("issues", null, null, null, null, null, null);
//        while (cr.moveToNext()) {
//            String uParentProject = cr.getString(cr.getColumnIndex("id_issue"));
//            idIssueParent.add(Integer.valueOf(uParentProject));
//        }
//        for (int i = 0; i < idIssueParent.size(); i++) {
//            Log.d(LOG_TAG, "--- idIssueParent " + idIssueParent.get(i));
//            Log.d(LOG_TAG, "--- idChooseIssue " + idIssue);
//            Log.d(LOG_TAG, "--- equals?  " + idIssueParent.get(i).equals(Integer.valueOf(idIssue)));
//            if (idIssueParent.get(i).equals(Integer.valueOf(idIssue))) {
//                flag = true;
//            } //else flag = false;
//        }
//        cr.close();
//        dbHelper.close();
//
//    }

    public void ifParentId() {
        db = dbHelper.getWritableDatabase();
        idParent = new ArrayList();
        flag = false;
        Cursor cr;
        cr = db.query("issues", null, null, null, null, null, null);
        while (cr.moveToNext()) {
            String uParentProject = cr.getString(cr.getColumnIndex("id_parent_project"));
            idParent.add(Integer.valueOf(uParentProject));
        }
        for (int i = 0; i < idParent.size(); i++) {
            if (idParent.get(i).equals(Integer.valueOf(idS))) {
                flag = true;
            } else flag = false;
        }
        cr.close();
        dbHelper.close();

    }

    public void fillingIssuesfromDB() {

        db = dbHelper.getWritableDatabase();
        Cursor cr;

        List<String> subject = new ArrayList();
        List<Integer> idIssue = new ArrayList();
        List<Integer> idParentProject = new ArrayList();
        cr = db.query("issues", null, null, null, null, null, null);
        while (cr.moveToNext()) {
            String uname = cr.getString(cr.getColumnIndex("subject"));
            subject.add(uname);
            String uId = cr.getString(cr.getColumnIndex("id_issue"));
            idIssue.add(Integer.valueOf(uId));
            String uParentProject = cr.getString(cr.getColumnIndex("id_parent_project"));
            idParentProject.add(Integer.valueOf(uParentProject));
        }
        Log.d("MyLogsP", "--- IDS + idParentProject---" + idS + " " + (String.valueOf(idParentProject.get(0))));
        for (int m = 0; m < idParentProject.size(); m++) {   //?????? check this function
            Log.d("MyLogsP", "--- IDS + idParentProject---" + Integer.valueOf(idS) + " " + idParentProject.get(m) + " equals or not?  " + ((idParentProject.get(m)).equals(Integer.valueOf(idS))));
            if ((idParentProject.get(m)).equals(Integer.valueOf(idS))) {
                Log.d("MyLogsP", "--- in cycle for if idParent project equals idS ---");
                Log.d("MyLogsP", "--- IDS + idParentProject---" + idS + " " + (String.valueOf(idParentProject.get(m))));
                IssueModel mv = new IssueModel();
                mv.setSubject(subject.get(m));
                mv.setId(idIssue.get(m));
                issueALL.add(mv);
                Log.d("MyLogsP", "--- issueAll size after filling ---" + issueALL.size());
            } else {
                Toast.makeText(context, "No loading information", Toast.LENGTH_SHORT).show();
            }
        }
        cr.close();
        dbHelper.close();
    }

    @Override
    public void onStop() {
        super.onStop();
        this.getActivity().findViewById(R.id.pager).setVisibility(View.VISIBLE);
    }

    public interface onClickIssueListener {
        void chooseIssue(String id, String author, String description, String subject, String status, String priority);

        void createNewIssue();
    }

    @OnClick(R.id.imageButton2)
    public void createNewIssue() {
        onClickIssueListener.createNewIssue();
    }

    @OnClick(R.id.nextBtn)
    public void loadingNext() {
        if (issueALL.size() < totalCount) {
            Log.d("Total count ", "total count " + totalCount);
            Log.d("Total count ", "issue All " + issueALL.size());
            newHttpRequest();
        } else {
            Toast.makeText(context, "There are no more components", Toast.LENGTH_LONG).show();
        }

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
