package ru.startandroid.fragmentapp.fragments;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v4.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnTextChanged;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ru.startandroid.fragmentapp.activities.MainActivity;
import ru.startandroid.fragmentapp.database.DBHelperNew;
import ru.startandroid.fragmentapp.R;
import ru.startandroid.fragmentapp.models.ProjectModel;
import ru.startandroid.fragmentapp.modelsredmine.ProjectModelRequest;
import ru.startandroid.fragmentapp.retrofit.TheApplication;
import ru.startandroid.fragmentapp.ui.ProjectRecyclerViewAdapter;


public class ProjectFragment extends Fragment implements ProjectRecyclerViewAdapter.ItemClickListener {


    public List<ProjectModel> mvList = new ArrayList<>();

    @BindView(R.id.rvProjects)
    RecyclerView rv;

    @BindView(R.id.etSearch)
    EditText etSearch;

    @BindView(R.id.imageButton3)
    ImageButton imgButton;

    @BindView(R.id.hellotext)
    TextView hellotext;

    Unbinder unbinder;
    DBHelperNew dbHelper;
    SQLiteDatabase db;
    ProjectRecyclerViewAdapter adapter;
    Context context;
    onClickProjectListener onClickProjectListener;

    final static String LOG_TAG = "MyLogsDB";


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        onClickProjectListener = (onClickProjectListener) context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mvList.clear();
        View view = inflater.inflate(R.layout.fragment_project, null);
        context = view.getContext();
        unbinder = ButterKnife.bind(this, view);
        dbHelper = new DBHelperNew(context);
        if (isOnline(context)) {
        } else {
            db = dbHelper.getWritableDatabase();
            Cursor c = db.query("project", null, null, null, null, null, null);
            logCursor(c);
        }


        return view;

    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        List<ProjectModel> mvList = new ArrayList<>();
        db = dbHelper.getWritableDatabase();//??
        httpRequest();
        setAdapter(mvList);

    }

    @Override
    public void onResume() {
        super.onResume();

    }

    public void dataBase() {
        // connect to DataBase
        db = dbHelper.getWritableDatabase();
        // Log.d(LOG_TAG, "--- Clear mytable: ---");
        // удаляем все записи
        if (isOnline(context)) {
            int clearCount = db.delete("project", null, null);
            Log.d(LOG_TAG, "deleted rows count = " + clearCount);
        }
        // checking if there are some content in db
        Cursor c = db.query("project", null, null, null, null, null, null);
        if (c.getCount() == 0) {
            ContentValues cv = new ContentValues();
            // filling our table with data
            for (int i = 0; i < mvList.size(); i++) {
                cv.put("proj_name", mvList.get(i).getName());
                cv.put("id", mvList.get(i).getId());
                db.insert("project", null, cv);
            }
        }
        Log.d(LOG_TAG, "--- Все записи Project Table---");
        c = db.query("project", null, null, null, null, null, null);
        logCursor(c);
        c.close();
        dbHelper.close();
    }


    void logCursor(Cursor c) {
        if (c != null) {
            if (c.moveToFirst()) {
                String str;
                do {
                    str = "";
                    for (String cn : c.getColumnNames()) {
                        str = str.concat(cn + " = " + c.getString(c.getColumnIndex(cn)) + "; ");
                    }
                    Log.d(LOG_TAG, str);
                } while (c.moveToNext());
            }
        } else
            Log.d(LOG_TAG, "Cursor is null");
    }

    public void fillingfromDB() {
        db = dbHelper.getWritableDatabase();
        Cursor cr;
        // List<ProjectModel> proMod = new ArrayList<>();
        List<String> name = new ArrayList();
        List<Integer> id = new ArrayList();
        cr = db.query("project", null, null, null, null, null, null);
        while (cr.moveToNext()) {
            String uname = cr.getString(cr.getColumnIndex("proj_name"));
            name.add(uname);
            String uId = cr.getString(cr.getColumnIndex("id"));
            id.add(Integer.valueOf(uId));
        }
        Log.d("MyLogsDB", "proj name in array" + name.toString());
        Log.d("MyLogsDB", "id in array" + id.toString());
        for (int i = 0; i < name.size(); i++) {
            ProjectModel mv = new ProjectModel();
            mv.setName(name.get(i));
            mv.setId(id.get(i));
            mvList.add(mv);
        }
        Log.d("MyLogs", "offline MVLIST " + mvList.toString() + "  " + mvList.size());
        cr.close();
        dbHelper.close();
    }

    public void setAdapter(List<ProjectModel> mData) {
        adapter = new ProjectRecyclerViewAdapter(context, mData);
        LinearLayoutManager llm = new LinearLayoutManager(context);
        rv.setLayoutManager(llm);
        adapter.setClickListener(this);
        rv.setAdapter(adapter);

        Log.d("pop", " set adapter");
    }

    public static boolean isOnline(Context context) {
        ConnectivityManager cm =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            return true;
        }
        return false;
    }

    public void hellouser() {
        int currentTime = Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
        if ((6 <= currentTime) && (currentTime < 12)) {
            hellotext.setText("Good Morning, Mariia. You have " + mvList.size() + " projects");
        }
        if ((12 <= currentTime) && (currentTime < 18)) {
            hellotext.setText("Good Day, Mariia. You have " + mvList.size() + " projects");
        }
        if ((18 <= currentTime) && (currentTime < 22)) {
            hellotext.setText("Good evening, Mariia. You have " + mvList.size() + " projects");
        }
        if ((22 <= currentTime) && (currentTime < 6)) {
            hellotext.setText("Good Night, Mariia. You have " + mvList.size() + " projects");
        }
        Log.d("MyLogs", "data time " + currentTime);
    }


    @Override
    public void onItemClick(View view, int position) {
        onClickProjectListener.chooseProject(String.valueOf(adapter.getItem(position).getId()));
    }

    public void httpRequest() {
        TheApplication.getApi().getData(MainActivity.KEY).enqueue(new Callback<ProjectModelRequest>() {
            @Override
            public void onResponse(Call<ProjectModelRequest> call, Response<ProjectModelRequest> response) {
                mvList.addAll(response.body().getProjects());
                rv.getAdapter().notifyDataSetChanged();
                setAdapter(mvList);
                hellouser();
                dataBase();
            }

            @Override
            public void onFailure(Call<ProjectModelRequest> call, Throwable t) {
                Toast.makeText(context, "An error occurred during networking", Toast.LENGTH_SHORT).show();
                fillingfromDB();
                hellouser();
                setAdapter(mvList);
            }
        });
    }


    public interface onClickProjectListener {
        void chooseProject(String s);
    }

    @OnTextChanged(R.id.etSearch)
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        List<ProjectModel> massRes = new ArrayList<>();
        for (ProjectModel project : mvList) {
            String projname = project.getName();
            if (projname.toLowerCase().contains(s) || String.valueOf(project.getId()).contains(s)) {
                massRes.add(project);
            }
        }
        adapter.filterList(massRes);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
