package ru.startandroid.fragmentapp.fragments;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import ru.startandroid.fragmentapp.R;

public class DetailIssueFragment extends DialogFragment {

    View view;
    String idIssue, author, description, subject, status, priority;
    Unbinder unbinder;

    @BindView(R.id.tvIdIssue)
    TextView tvIdIssue;
    @BindView(R.id.tvPriority)
    TextView tvPriority;
    @BindView(R.id.tvStatus)
    TextView tvStatus;
    @BindView(R.id.tvAuthor)
    TextView tvAuthor;
    @BindView(R.id.tvTrackername)
    TextView tvTrackername;
    @BindView(R.id.tvDescription)
    TextView tvDescription;


    public DetailIssueFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getDialog().setTitle("Issue's details: ");
        view = inflater.inflate(R.layout.fragment_detail_issue, null);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        idIssue = this.getArguments().getString("idIssue");
        author = this.getArguments().getString("Author");
        description = this.getArguments().getString("Description");
        subject = this.getArguments().getString("Subject");
        status = this.getArguments().getString("Status");
        priority = this.getArguments().getString("Priority");
        tvAuthor.append(author);
        tvTrackername.append(subject);
        tvPriority.append(priority);
        tvStatus.append(status);
        tvIdIssue.append(idIssue);
        if (description == null || description.isEmpty()) {
            tvDescription.setText("description: no any description");
        } else {
            tvDescription.setText("description: " + description);
        }

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
