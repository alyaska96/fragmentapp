package ru.startandroid.fragmentapp.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnItemClick;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ru.startandroid.fragmentapp.activities.MainActivity;
import ru.startandroid.fragmentapp.R;
import ru.startandroid.fragmentapp.models.ProjectModel;
import ru.startandroid.fragmentapp.modelsredmine.ProjectModelRequest;
import ru.startandroid.fragmentapp.retrofit.TheApplication;
import ru.startandroid.fragmentapp.ui.ProjectGridAdapter;


public class GridProjectFragment extends Fragment {

    Context context;
    Unbinder unbinder;

    @BindView(R.id.gvMain)
    GridView gvMain;
    ProjectGridAdapter gridAdapter;

    public List<ProjectModel> pmGridList = new ArrayList<>();
    private onClickProjectListener onClickProjectListener;

    public GridProjectFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        pmGridList.clear();
        View view = inflater.inflate(R.layout.fragment_grid_project, null);
        context = view.getContext();
        unbinder = ButterKnife.bind(this, view);
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                httpRequest();
            }
        }, 500);
        setGridAdapter(pmGridList);
        adjustGridView();
        return view;
    }

    @OnItemClick(R.id.gvMain)
    public void chooseProject(int position) {
        onClickProjectListener.chooseGridProject(String.valueOf(gridAdapter.getProj(position).getId()));
    }
    @Override
    public void onAttach(Context activity) {

        super.onAttach(activity);
        try {
            onClickProjectListener = (onClickProjectListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement onSomeEventListener");
        }
    }


    public void httpRequest() {
        TheApplication.getApi().getData(MainActivity.KEY).enqueue(new Callback<ProjectModelRequest>() {
            @Override
            public void onResponse(Call<ProjectModelRequest> call, Response<ProjectModelRequest> response) {
                pmGridList.addAll(response.body().getProjects());
                gridAdapter.filterList(pmGridList);
            }

            @Override
            public void onFailure(Call<ProjectModelRequest> call, Throwable t) {
                Toast.makeText(context, "An error occurred during networking", Toast.LENGTH_SHORT).show();
                Log.d("MyLogs", "On failure" + t.getMessage());
            }
        });
        Log.d("MyLogs", "mvList Grid Fragment" + pmGridList.size());
    }

    private void adjustGridView() {
        gvMain.setNumColumns(1);
        gvMain.setVerticalSpacing(10);
    }

    public void setGridAdapter(List<ProjectModel> mData) {
        gridAdapter = new ProjectGridAdapter(context, mData);
        gvMain.setAdapter(gridAdapter);
        Log.d("MyLogs", "grid adapter is set");

    }

    @Override
    public void onResume() {
        super.onResume();

    }

    public interface onClickProjectListener {
        void chooseGridProject(String s);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
